#pragma checksum "D:\Nag\NagTwitterAPI\NagTwitterAPI\Views\Home\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "433364bbd4798f2ae268ffab2ac3919998a52270"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Index), @"mvc.1.0.view", @"/Views/Home/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\Nag\NagTwitterAPI\NagTwitterAPI\Views\_ViewImports.cshtml"
using NagTwitterAPI;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\Nag\NagTwitterAPI\NagTwitterAPI\Views\_ViewImports.cshtml"
using NagTwitterAPI.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"433364bbd4798f2ae268ffab2ac3919998a52270", @"/Views/Home/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"45b8e147bb398fb675783e416d0b1762f32eb1bf", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 2 "D:\Nag\NagTwitterAPI\NagTwitterAPI\Views\Home\Index.cshtml"
  
    ViewData["Title"] = "Home Page";

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
<div>
    <h3 class=""display-4"">Twitter Search</h3>
</div> <br /> <br />
<div class=""row"">
    <div class=""col-md-4"">
        <input type=""text"" id=""searchText"" width=""200"" class=""form-control"" />
    </div>
    <div class=""col-md-2"">
        <input type=""button"" id=""btnSearch"" class=""btn btn-primary"" value=""Search"" />
    </div>
    <div class=""col-md-6"">

    </div>
</div>
<br />
<div id=""searchResults"">

</div>

");
            DefineSection("scripts", async() => {
                WriteLiteral("\r\n    <script type=\"text/javascript\">\r\n    $(document).ready(function(){\r\n\r\n        var url = \'");
#nullable restore
#line 29 "D:\Nag\NagTwitterAPI\NagTwitterAPI\Views\Home\Index.cshtml"
              Write(Url.Action("DisplaySearchResults", "Search"));

#line default
#line hidden
#nullable disable
                WriteLiteral("\';\r\n        $(\'#btnSearch\').click(function() {\r\n            var keyWord = $(\'#searchText\').val();\r\n            $(\'#searchResults\').load(url, { searchKey: keyWord });\r\n        })\r\n\r\n    });\r\n\r\n    </script>\r\n");
            }
            );
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
