#pragma checksum "D:\Nag\NagTwitterAPI\NagTwitterAPI\Views\Shared\_SearchResults.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "e4f896dc5d4ee1c8ca86a075ed961933a32f53b4"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Shared__SearchResults), @"mvc.1.0.view", @"/Views/Shared/_SearchResults.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\Nag\NagTwitterAPI\NagTwitterAPI\Views\_ViewImports.cshtml"
using NagTwitterAPI;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\Nag\NagTwitterAPI\NagTwitterAPI\Views\_ViewImports.cshtml"
using NagTwitterAPI.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 1 "D:\Nag\NagTwitterAPI\NagTwitterAPI\Views\Shared\_SearchResults.cshtml"
using Tweetinvi.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"e4f896dc5d4ee1c8ca86a075ed961933a32f53b4", @"/Views/Shared/_SearchResults.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"45b8e147bb398fb675783e416d0b1762f32eb1bf", @"/Views/_ViewImports.cshtml")]
    public class Views_Shared__SearchResults : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<List<ITweet>>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n    <div class=\"col-12\">\r\n");
#nullable restore
#line 5 "D:\Nag\NagTwitterAPI\NagTwitterAPI\Views\Shared\_SearchResults.cshtml"
         if (Model != null)
        {

#line default
#line hidden
#nullable disable
            WriteLiteral("            <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\r\n\r\n");
#nullable restore
#line 9 "D:\Nag\NagTwitterAPI\NagTwitterAPI\Views\Shared\_SearchResults.cshtml"
                 foreach (ITweet tweet in Model)
                {

#line default
#line hidden
#nullable disable
            WriteLiteral("                <tr>\r\n                    <td>");
#nullable restore
#line 12 "D:\Nag\NagTwitterAPI\NagTwitterAPI\Views\Shared\_SearchResults.cshtml"
                   Write(tweet.CreatedBy);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                </tr>\r\n                <tr>\r\n                    <td>");
#nullable restore
#line 15 "D:\Nag\NagTwitterAPI\NagTwitterAPI\Views\Shared\_SearchResults.cshtml"
                   Write(tweet.TweetLocalCreationDate.ToString());

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                </tr>\r\n                <tr>\r\n                    <td><p>");
#nullable restore
#line 18 "D:\Nag\NagTwitterAPI\NagTwitterAPI\Views\Shared\_SearchResults.cshtml"
                      Write(tweet.Text);

#line default
#line hidden
#nullable disable
            WriteLiteral("</p></td>\r\n                </tr>\r\n");
#nullable restore
#line 20 "D:\Nag\NagTwitterAPI\NagTwitterAPI\Views\Shared\_SearchResults.cshtml"
                }

#line default
#line hidden
#nullable disable
            WriteLiteral("            </table>\r\n");
#nullable restore
#line 22 "D:\Nag\NagTwitterAPI\NagTwitterAPI\Views\Shared\_SearchResults.cshtml"
        }

#line default
#line hidden
#nullable disable
            WriteLiteral("    </div>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<List<ITweet>> Html { get; private set; }
    }
}
#pragma warning restore 1591
