﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Tweetinvi;
using Tweetinvi.Models;
using Tweetinvi.Parameters;

namespace NagTwitterAPI.Controllers
{
    public class SearchController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult DisplaySearchResults(string searchKey)
        {
            var model = getPublishedTweets(searchKey);
            return PartialView("_SearchResults", model);
        }

        public List<ITweet> getPublishedTweets(string searchKey)
        {
            try
            {
                Auth.SetUserCredentials(AccountConfig.CONSUMER_KEY, AccountConfig.CONSUMER_SECRET, AccountConfig.ACCESS_TOKEN, AccountConfig.ACCESS_TOKEN_SECRET);

                var searchParameter = new SearchTweetsParameters(searchKey.Trim())
                {
                    Lang = LanguageFilter.English,
                    SearchType = SearchResultType.Recent,
                    MaximumNumberOfResults = 10,
                    Until = new DateTime(2020, 03, 23),
                    Filters = TweetSearchFilters.Images | TweetSearchFilters.Verified
                };

                return Search.SearchTweets(searchParameter).OrderByDescending(q => q.CreatedAt).ToList();
            }
            catch
            {
                return null;
            }
        }

    }
}